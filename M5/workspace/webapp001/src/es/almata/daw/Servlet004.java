package es.almata.daw;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet004
 */
@WebServlet("/Servlet004")
public class Servlet004 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	String sCadena1 = new String("lucas");
	String psd = new String("1234");
	int i=0;
	int e=2;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet004() {
        super();
        // TODO Auto-generated constructor stub
    }

   /* 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw= response.getWriter();
		pw.println(request.getParameter("usuari"));
		pw.println(request.getParameter("pwd"));
		pw.close();
	}
	*/
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw= response.getWriter();
		pw.println("<html>");
		pw.println("<body>");
		if(sCadena1.equals(request.getParameter("usuari")) && psd.equals(request.getParameter("pwd"))) {
		pw.println("<form action=index.html method=post >");
		pw.println("<p>"+"Tu nombre de usuario es: "+request.getParameter("usuari")+"</P>");
		pw.println("<p>"+"Tu pwd es: "+request.getParameter("pwd")+"</P>");
		pw.println("<input type=submit value=CanviarPwd />");
		pw.println("</form>");
		}else{
			if(i<=2) {
				pw.println("<form action=Formulari003.html method=post >");
				pw.println("<p>"+"Usuario o password incorrecto"+"</p>");
				pw.println("<input type=submit value=Torna-hi />");
				pw.println("</form>");				
				pw.println("<p>"+"Et queden "+e+" intents"+"</p>");
				e--;
				i++;
			}else{
				pw.println("Massa intents realitzats, espera un temps per tornar a intentar-ho");
			}						
		}		
		pw.println("</body>");
		pw.println("</html>");
		pw.close();
	}	

}
