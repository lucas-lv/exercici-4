package es.almata.daw;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet005
 */
@WebServlet("/Servlet005")
public class Servlet005 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String psd = new String("1234");
	String psd2 = new String("2222");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet005() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    /*
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw= response.getWriter();
		if(psd.equals(request.getParameter("pwd"))){
			if(psd2.equals(request.getParameter("pwd2")) && psd2.equals(request.getParameter("pwd3"))) {
			pw.println("<html>");
			pw.println("<body>");
			pw.println("<p>"+"La nova contrasenya es: "+request.getParameter("pwd2")+"</p>");
			}else {
				pw.println("<p>"+"Error al auntenticar la nova contrasenya"+"</p>");
				pw.println("<form action=index.html method=post >");
				pw.println("<input type=submit value=Torna-hi />");
				pw.println("</form>");
			}
		}else{
			pw.println("<p>"+"La contrasenya antiga no es correcta"+"</p>");
			pw.println("<form action=index.html method=post >");
			pw.println("<input type=submit value=Torna-hi />");
			pw.println("</form>");			
		}
		pw.println("</body>");
		pw.println("</html>");
		pw.close();
	}

}
